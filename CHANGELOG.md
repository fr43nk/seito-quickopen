## 1.5.0

Fixed the file system event based list update

## 1.4.1

Ignoring case of search string

## 1.4.0

Added maintain task for file cache

## 1.3.10

Fixed performance issue while input search pattern.

## 1.3.9

Fixed handling of non existing workspace folders (#4).
Search didn't end correctly and index wasn't updated.

## 1.3.8

Internal changes

## 1.3.7

Fixed console log output

## 1.3.6

Fixed abort handling and storage of new files

## 1.3.5

Change the abort command

## 1.3.3

Adding extension icon

## 1.3.2

Adding octicons

## 1.3.1

Issue in selecting the right OS type.

## 1.3.0

Performance tweaks.
* use Windows command `dir` or Linux command `find` for file search
* leads to more responsive UI

## 1.2.1

Minor recent file list tweaks

## 1.2.0

Added recent list in open dialog

## 1.1.0

Added new configuration date `use-file-events`
- listen to file creation events within workspace to update cache

## 1.0.4

Quick open dialog now only displays file and path without detail.

## 1.0.3

Fix building of first run file cache (did not work)

## 1.0.2

Minor fix

## 1.0.1

Minor fix

## 1.0.0

First release of extension