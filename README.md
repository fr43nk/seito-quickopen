# VSCode Quick Open

A VSCode extension for quick file open.
Sometimes the build in action for quickopen doesn't work or you need some kind of file search cache.
So you can use this extension for a better user experience.
It uses the `search.exclude` configuration to filter what is found.

Type `Quick File Open` in the command input to get the file index.
For better usage, create a keyboard shortcut.

Once the search is ready, the file list gets stored in the workspace state.
This is done to increase performance when reopen the workspace.

For instant update on file system activity one can enable the file system events in the extension configuration.

```
seito-quickopen.use-file-events: true
```
