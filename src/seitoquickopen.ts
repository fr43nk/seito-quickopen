'use strict';

import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as cp from 'child_process';
import * as os from 'os';
import ignore from 'ignore';
import { ListItem } from './types/list-item';
import { AbortToken, AbortState_e } from './types/abort-token';
import { ConfigurationHandler } from './configuration-handler';
import { ModelHandler } from './model-handler';
import { Storage } from './storage';
import { Subject, Observable } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

let storage: Storage;
let outputCh = vscode.window.createOutputChannel('Seito QuickOpen');
let ignores = ignore();
let statusbarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 10);
let quickPicDialog: vscode.QuickPick<ListItem> = vscode.window.createQuickPick();
let isFirstRun = true;
let theContext: vscode.ExtensionContext;
let abortSearch: AbortToken = new AbortToken();
let isRunning = false;
let configHandler: ConfigurationHandler;
let modelHandler: ModelHandler;
let maintainInterval: any;
let locker: number = 0;
const disposables: vscode.Disposable[] = [];
const obsv: Subject<string> = new Subject<string>();

const LIST_COMMAND_FILE = ((os.type() === "Windows_NT") ? 'cmd.exe' : 'find');



export function activate(context: vscode.ExtensionContext) {
  theContext = context;
  setupComponents();
  setupWatcher();

  configHandler.onDidChangeConfiguration((changedData) => {
    if (configHandler.configuration.UseFileEvents.Value === true) {
      setupWatcher();
    } else {
      modelHandler.deleteWatcher();
    }
  });

  vscode.workspace.onDidOpenTextDocument((doc) => {
    storage.addToRecent(doc.uri.fsPath);
    findAndUpdateCache(doc.uri);
  });
  vscode.workspace.onDidSaveTextDocument((doc) => {
    findAndUpdateCache(doc.uri);
  });
  vscode.workspace.onDidChangeConfiguration((e) => {
    if (e.affectsConfiguration("search") === true) {
      if (isRunning === false) {
        setupExcludes();
        getAllWorkspaceFolderFiles();
      }
      else {
        abortSearch.doAbortAndRestart();
      }
    }
  });

  abortSearch.onAbort().subscribe((val:AbortState_e) => {
    if( val === AbortState_e.FinishDoRestart ) {
      setupExcludes();
      getAllWorkspaceFolderFiles();
    }
  });

  obsv.pipe(debounceTime(600)).pipe(switchMap((val) => filterFileName(val))).subscribe((items) => {
    if (items.length > 0)
      quickPicDialog.items = items;
    quickPicDialog.busy = false;
  });

  quickPicDialog.matchOnDescription = true;
  quickPicDialog.matchOnDetail = true;
  quickPicDialog.onDidChangeValue((pattern) => {
    quickPicDialog.placeholder = `Searching ${storage.Size} files`;
    quickPicDialog.busy = true;
    if (quickPicDialog.value === "") {
      quickPicDialog.items = getRecentFileList();
    }
    obsv.next(pattern);
  });

  quickPicDialog.onDidAccept(() => {
    let items = quickPicDialog.selectedItems;
    for (let item of items) {
      if (false === deleteIfNotExists(item)) {
        const selectedFile = vscode.Uri.file(item.fullPath);
        vscode.workspace.openTextDocument(selectedFile).then((document) => {
          vscode.window.showTextDocument(document);
        });
        quickPicDialog.hide();
      }
    }
  });

  setupExcludes();
  // restore old cache
  let fileList = context.workspaceState.get('filecache', []);
  isFirstRun = (fileList.length !== undefined && fileList.length === 0);
  if (isFirstRun) {
    storage.setIsNewStorage();
  }
  storage.parse(fileList);
  
  
  let searchCommmand = vscode.commands.registerCommand('extension.seitoQuickOpen', async () => {
    quickPicDialog.placeholder = `Searching ${storage.Size} files`;
    quickPicDialog.items = getRecentFileList();
    quickPicDialog.value = "";
    quickPicDialog.show();
  });
  
  let rebuildCommand = vscode.commands.registerCommand('extension.seitoQuickOpenRebuild', () => {
    getAllWorkspaceFolderFiles();
  });

  let abortCommand = vscode.commands.registerCommand('extension.seitoQuickOpenAbort', () => {
    abortSearch.doAbort();
  });
  
  context.subscriptions.push(searchCommmand);
  context.subscriptions.push(rebuildCommand);
  context.subscriptions.push(abortCommand);

  statusbarDefaults();
  setTimeout(getAllWorkspaceFolderFiles, 10000);
  maintainInterval = setInterval(maintainFileCache, 120000);

  console.log('[seito-quickopen] activated!');
}

export function setupComponents() {
  configHandler = new ConfigurationHandler(disposables);
  modelHandler = new ModelHandler();
  storage = new Storage();
}

async function findAndUpdateCache(uriObj: vscode.Uri) {
  try {
    let file = uriObj.fsPath;
    fs.accessSync(file);
    let state = fs.statSync(file);
    if (state.isFile() === true) {
      let f = path.basename(file);
      let d = path.dirname(file);
      let doAdd = false;
      if (vscode.workspace.workspaceFolders !== undefined) {
        for (let w of vscode.workspace.workspaceFolders) {
          if (d.indexOf(w.uri.fsPath) !== -1) {
            doAdd = true;
            break;
          }
        }
        if (doAdd === true && storage.hasValue(file) !== true && ignores.ignores(file) === false) {
          storage.addToActive(file, f);
        }
      }
    }
  } catch (err) {
    ;
  }
}

async function onCreateFileDoUpdate(fileObj: vscode.Uri) {
  if (storage.hasValue(fileObj.fsPath) === false) {
    storage.addToActive(fileObj.fsPath, path.basename(fileObj.fsPath));
  }
}

async function onDeleteFileDoUpdate(fileObj: vscode.Uri) {
  if (storage.hasValue(fileObj.fsPath) === true) {
    storage.deleteValue(fileObj.fsPath);
    storage.removeFromRecent(fileObj.fsPath);
  }
}

function deleteIfNotExists(li: ListItem) {
  try {
    if (li !== undefined)
      fs.accessSync(li.fullPath);
    return false;
  } catch (err) {
    storage.deleteValue(li.fullPath);
  }
  return true;
}

export async function getAllWorkspaceFolderFiles() {
  if (vscode.workspace.workspaceFolders !== undefined) {
    outputCh.appendLine("Fetching all files in Workspace");
    isRunning = true;
    statusbarRunning();
    let proms = vscode.workspace.workspaceFolders.map((val => { return getWorkspaceFolderFileList(val.uri) }));
    Promise.all(proms).then(() => {
      isRunning = false;
      if(abortSearch.Abort === AbortState_e.None) {
        statusbarItem.text = `$(verified) File search ready`;
        storage.activate();
        theContext.workspaceState.update("filecache", storage.serialize());
      }
      abortSearch.doAbortFinish();
      statusbarDefaults();
    }, (reason) => {
      outputCh.appendLine(`Rejecting: ${reason}`);
    });
  }
}

function statusbarDefaults() {
  statusbarItem.text = `$(sync)`;
  statusbarItem.tooltip = "Start rebuilding file cache";
  statusbarItem.command = "extension.seitoQuickOpenRebuild";
  statusbarItem.show();
}

function statusbarRunning() {
  statusbarItem.tooltip = "Busy building file cache. Click to abort";
  statusbarItem.text = `$(info) Building cache`;
  statusbarItem.command = "extension.seitoQuickOpenAbort";
}

export function getStorage() : Storage {
  return storage;
}

export async function getWorkspaceFolderFileList(uriObj: vscode.Uri) {
  outputCh.appendLine(`Fetching files in ${uriObj.fsPath}`);
  try{
    await readdirRecurse(uriObj.fsPath);
    outputCh.appendLine(`Finished fetching files in ${uriObj.fsPath}`);
    statusbarItem.text = `${uriObj.fsPath} ready`;
  } catch(err) {
    outputCh.appendLine(`${uriObj.fsPath} finished with error: ${err.message}`);
    statusbarItem.text = `${uriObj.fsPath} finished with error!`;
  }
}

function getPathItems(items: string[], basePath: string) {
  for (let item of items) {
    if (abortSearch.Abort !== AbortState_e.None)
      break;
    if (ignores.ignores(path.relative(basePath, item)) === false) {
      let b = path.basename(item);
      storage.add(item, b);
    }
  }
}

function filterFileName(pattern: string): Observable<ListItem[]> {
  return new Observable((obsv) => {
    let filtered: ListItem[] = [];
    if (pattern !== "") {
      let p = pattern.toLowerCase();
      storage.Active.forEach((val, key) => {
        if (val.toLowerCase().indexOf(p) !== -1) {
          filtered.push(new ListItem(key));
        }
      });
    }
    obsv.next(filtered);
  })
}

function getRecentFileList(): ListItem[] {
  let filtered: ListItem[] = [];
  for (let p of storage.Recent) {
    filtered.push(new ListItem(p));
  }
  return filtered;
}

function readdirRecurse(basePath: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
		let args = [basePath, "-type", "f"];
		if (os.type() === "Windows_NT") {
			args = ["/c", "dir", "/A-D", "/S", "/B", basePath];
    }
    const command = cp.spawn(LIST_COMMAND_FILE, args, { cwd: basePath, env: process.env });

    if (command !== null) {
      if (command.stdout !== null) {
        command.stdout.on("data", (data) => {
          if (typeof data === 'string') {
            getPathItems(data.split(/\r\n|\r|\n/).filter(s => s.length > 0), basePath);
          } else {
            getPathItems(data.toString().split(/\r\n|\r|\n/).filter((s:string) => s.length > 0), basePath);
          }
        });
      }

      command.on("error", (err: Error) => {
        reject(err);
      })

      command.on('close', (code, signal) => {
        resolve();
      });

      let subs = abortSearch.onAbort().subscribe((val:AbortState_e) => {
        if(val === AbortState_e.Abort || val === AbortState_e.AbortAndRestart) {
          command.kill();
          subs.unsubscribe();
        }
      })
    }
    else {
      reject(Error("Command could not be startet"));
    }
  });
}

export function setupExcludes() {
  let searchPatterns: string[] = [];
  if (vscode.workspace.getConfiguration("search", null).has("exclude")) {
    let searchExcludes = vscode.workspace.getConfiguration("search", null).get("exclude")!;
    if (searchExcludes !== undefined) {
      searchPatterns = Object.keys(searchExcludes).filter((val) => {
        if (vscode.workspace.getConfiguration("search").get(`exclude.${val}`) !== false)
          return val;
      });
    }
  }
  ignores = ignore();
  ignores.add(searchPatterns);
}

export function setupWatcher() {
  let useEvents = configHandler.configuration.UseFileEvents.Value;
  let m = modelHandler.addWatcher('**', !useEvents, !useEvents, !useEvents);
  m.onWorkspaceCreated(onCreateFileDoUpdate);
  m.onWorkspaceDeleted(onDeleteFileDoUpdate);
  m.onWorkspaceChanged(findAndUpdateCache);
}

export function deactivate(context: vscode.ExtensionContext) {
  storage.dispose();
  context.workspaceState.update('filecache', []);
  clearInterval(maintainInterval);
}

async function maintainFileCache() {
  if( (locker++ === 1) ) {
    let keys = storage.getValues().keys();
    let entry = keys.next().value;
    while(entry !== undefined && entry !== null) {
      try{
        fs.accessSync(entry);
      } catch(err) {
        storage.deleteValue(entry);
      }
      entry = keys.next().value;
    }
  }
  locker--;
}