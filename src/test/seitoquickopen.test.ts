import * as assert from 'assert';
import * as vscode from 'vscode';

import { getStorage, setupComponents, setupExcludes, setupWatcher, getAllWorkspaceFolderFiles, getWorkspaceFolderFileList } from "../seitoquickopen";
import { setupEnvironment, cleanEnvironment, WORKSPACE_DIRECTORIES, WORKSPACE_FILES, WORKSPACE_FILES_MODIFIED } from './setup';
import { writeFile, writeFileSync, watchFile, renameSync } from 'fs';
import { resolve } from 'dns';


let WS_ROOT = process.env.WS_ROOT;

suite("Quick open operations", () => {

	function wait(val: number): Promise<void> {
		return new Promise((resv, rej) => {
		setTimeout(() => {
			resv();
		}, val);
		});
	}

	suiteSetup((done) => {
		setupComponents();
		setupExcludes();

		setupEnvironment().then(done, done);
	})

	suiteTeardown((done) => {
		cleanEnvironment().then(done, done);
	})

	test("Get all files of workspace", () => {

		getWorkspaceFolderFileList(WORKSPACE_DIRECTORIES[0].uri).then(() => {
			let files = getStorage();
			files.activate();
			assert.equal(files.Size, WORKSPACE_FILES.length);
		});
	});

	test("Create a new file. Check if it is stored in list", () => {
		setupWatcher();

		getWorkspaceFolderFileList(WORKSPACE_DIRECTORIES[0].uri).then(() => {
			let storage = getStorage();
			storage.activate();
	
			// create file
			writeFileSync(WORKSPACE_FILES_MODIFIED[0].name, WORKSPACE_FILES_MODIFIED[0].content);
			wait(1000).then(() => {
				assert.equal(storage.hasValue(WORKSPACE_FILES_MODIFIED[0].name), true, "New file has been added to list");
			});
		})
	});

	test("Rename file. Check if it is stored in list", () => {
		setupWatcher();

		getWorkspaceFolderFileList(WORKSPACE_DIRECTORIES[0].uri).then(() => {
			let storage = getStorage();
			storage.activate();
	
			// create file
			writeFileSync(WORKSPACE_FILES_MODIFIED[0].name, WORKSPACE_FILES_MODIFIED[0].content);
			wait(1000).then(() => {
				renameSync(WORKSPACE_FILES_MODIFIED[0].name, WORKSPACE_FILES_MODIFIED[1].name);
				wait(1000).then(() => {
					assert.equal(!storage.hasValue(WORKSPACE_FILES_MODIFIED[0].name) &&
								 storage.hasValue(WORKSPACE_FILES_MODIFIED[1].name), true, "Old file has been removed from and new file has been added to list after rename");
				});
			});
		})
	});

	test("Move file. Check if it is stored in list", () => {
		setupWatcher();

		getWorkspaceFolderFileList(WORKSPACE_DIRECTORIES[0].uri).then(() => {
			let storage = getStorage();
			storage.activate();
	
			// create file
			writeFileSync(WORKSPACE_FILES_MODIFIED[0].name, WORKSPACE_FILES_MODIFIED[0].content);
			wait(1000).then(() => {
				renameSync(WORKSPACE_FILES_MODIFIED[0].name, WORKSPACE_FILES_MODIFIED[1].name);
				renameSync(WORKSPACE_FILES_MODIFIED[1].name, WORKSPACE_FILES_MODIFIED[2].name);
				wait(1000).then(() => {
					assert.equal(!storage.hasValue(WORKSPACE_FILES_MODIFIED[0].name) &&
								 !storage.hasValue(WORKSPACE_FILES_MODIFIED[1].name) &&
								 storage.hasValue(WORKSPACE_FILES_MODIFIED[2].name), true, "File has been moved to new location");
				});
			});
		})
	});
});
