import * as vscode from 'vscode';
import { mkdirSync, statSync, writeFileSync, unlinkSync, accessSync } from "fs";
import { sep } from "path";

let WS_ROOT = process.env.WS_ROOT;
export const DIRECTORIES = [
	WS_ROOT + sep + "TestTemp",
	WS_ROOT + sep + "TestTemp" + sep + "Folder1",
	WS_ROOT + sep + "TestTemp" + sep + "Folder2",
	WS_ROOT + sep + "TestTemp" + sep + "Folder3",
	WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1",
	WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_2",
	WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3",
	WS_ROOT + sep + "TestTemp" + sep + "Folder2" + sep + "Subfolder2_1",
	WS_ROOT + sep + "TestTemp" + sep + "Folder2" + sep + "Subfolder2_2",
	WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "Subfolder3_1",
	WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "Subfolder3_2",
	WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "Subfolder3_3",
	WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "Subfolder3_4",
];

export const WORKSPACE_DIRECTORIES: {uri: vscode.Uri, name?: string|undefined}[] = DIRECTORIES.map((val) => {
	return {uri: vscode.Uri.file(val), name: val};
});

export const WORKSPACE_FILES: {name: string, content: string}[] = [
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1" + sep + "file_1_1_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1" + sep + "file_1_1_2.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1" + sep + "file_1_1_3.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1" + sep + "file_1_1_4.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_2" + sep + "file_1_2_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3" + sep + "file_1_3_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3" + sep + "file_1_3_2.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3" + sep + "file_1_3_3.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3" + sep + "file_1_3_4.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3" + sep + "file_1_3_5.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_3" + sep + "file_1_3_6.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder2" + sep + "SubFolder2_1" + sep + "file_2_1_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder2" + sep + "SubFolder2_2" + sep + "file_2_2_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder2" + sep + "SubFolder2_2" + sep + "file_2_2_2.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "SubFolder3_1" + sep + "file_3_1_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "SubFolder3_2" + sep + "file_3_2_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "SubFolder3_3" + sep + "file_3_3_1.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder3" + sep + "SubFolder3_4" + sep + "file_3_4_1.txt", "content": "nix"}
];

export const WORKSPACE_FILES_MODIFIED: {name: string, content: string}[] = [
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1" + sep + "file_1_1_1_mod.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder1" + sep + "SubFolder1_1" + sep + "file_1_1_1_mods.txt", "content": "nix"},
	{"name": WS_ROOT + sep + "TestTemp" + sep + "Folder2" + sep + "SubFolder2_1" + sep + "file_1_1_1_mods.txt", "content": "nix"},
];

export function setupEnvironment(): Promise<MochaDone> {
	return new Promise((resolve, rejects) => {
		try {
			DIRECTORIES.forEach(dir => {
				try {
					let s = statSync(dir);
				} catch(e) {
					mkdirSync(dir);
				}
			});
			WORKSPACE_FILES.forEach(f => {
				writeFileSync(f.name, f.content);
			});
			resolve(undefined);
		}
		catch(e) {
			console.log(e.message);
			rejects(e);
		}
	});
}

export function cleanEnvironment(): Promise<MochaDone> {
	return new Promise((resolve, rejects) => {
		try {
			WORKSPACE_FILES.forEach(f => {
				unlinkSync(f.name);
			});
			WORKSPACE_FILES_MODIFIED.forEach(f => {
				try{
					accessSync(f.name);
					unlinkSync(f.name);
				}
				catch(e){;}
			});
			for(let i=DIRECTORIES.length; i<=0; i--) {
				let s = statSync(DIRECTORIES[i]);
				if( s.isDirectory() === true ) {
					unlinkSync(DIRECTORIES[i]);
				}
			}
			resolve(undefined);
		}
		catch(e) {
			console.log(e.message);
			rejects(e);
		}
	});
}

