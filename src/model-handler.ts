import { Model } from "./model";

export class ModelHandler {
	private m_models: Model[] = [];

	public constructor() {
		this.init();
	}

	public init() {
			this.m_models = [];
	}

	public addWatcher(filter: string = '**', ignoreCreated = false, ignoreChanged = false, ignoreDeleted = false): Model {
			let l_m = new Model();
			l_m.init(filter, ignoreCreated, ignoreChanged, ignoreDeleted);
			this.m_models.push(l_m);
			return l_m;
	}

	public deleteWatcher() {
		for(let w of this.m_models) {
			w.dispose();
		}
		this.init();
	}
}