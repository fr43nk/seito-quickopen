import { basename } from "path";
import { Disposable } from "vscode";
import { accessSync } from "fs";

const RECENT_FILE_LIST_LENGTH = 10;

export class Storage implements Disposable {
	private m_new: Map<string, string>;
	private m_old: Map<string, string>;
	private m_active: Map<string, string>;
	private m_recent: string[];
	private m_recentIdx: number;

	public constructor() {
		this.m_old = new Map<string, string>();
		this.m_new = new Map<string, string>();
		this.m_active = this.m_old;
		this.m_recent = [];
		this.m_recentIdx = 0;
	}

	public activate() {
		this.m_old = new Map<string, string>(this.m_new.entries());
		this.m_active = this.m_old;
		this.m_new.clear();
	}

	public setIsNewStorage() {
		this.m_active = this.m_new;
	}

	public add(key: string, val: string) {
		this.m_new.set(key, val);
	}

	public addToActive(key: string, val: string) {
		this.m_active.set(key, val);
	}

	public getValue(key: string): string | undefined {
		return this.m_active.get(key);
	}

	public hasValue(key: string): boolean {
		return this.m_active.has(key);
	}

	public deleteValue(key: string): boolean {
		return this.m_active.delete(key);
	}

	public get Size(): number {
		return this.m_active.size;
	}

	public get Active(): Map<string, string> {
		return this.m_active;
	}

	public serialize(): string[] {
		let list: string[] = [];
		this.m_active.forEach((val, key) => {
			list.push(key);
		});
		return list;
	}

	public parse(val: string[]) {
		if (val !== undefined && val !== null && val instanceof Array) {
			for (let e of val) {
				this.m_active.set(e, basename(e));
			}
		}
	}

	public addToRecent(val: string) {
		try {
			accessSync(val);
			// search entry
			let idx = 0;
			let found = false;
			for(let e of this.m_recent) {
				if(e === val) {
					this.m_recent.splice(idx, 1);
					this.m_recent.splice(0, 0, e);
					found = true;
					break;
				}
				idx++;
			}
			if( found === false ) {
				this.m_recent.splice(0,0,val);
				if( this.m_recent.length >= RECENT_FILE_LIST_LENGTH ) {
					this.m_recent.splice(this.m_recent.length-1, 1);
				}
			}
		} catch(e) {
			;
		}
	}

	public removeFromRecent(iFilename: string) {
		this.m_recent = this.m_recent.filter((val, idx) => val !== iFilename);
	}
	
	public getValues() {
		return this.Active;
	}

	public get Recent(): string[] {
		return this.m_recent;
	}

	public dispose() {
		this.m_old.clear();
		this.m_new.clear();
	}
}