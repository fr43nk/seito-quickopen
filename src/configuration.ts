'use strict';

import { ConfigurationProperty } from "./types/configuration-property";

export class Configuration
{
	private m_useFileEvents: ConfigurationProperty<boolean> = new ConfigurationProperty(false);

	public get UseFileEvents() : ConfigurationProperty<boolean> {
		return this.m_useFileEvents;
	}
}