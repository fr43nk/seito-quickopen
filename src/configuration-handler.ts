'use strict';

import * as vscode from 'vscode';
import { Configuration } from './configuration';
import { ConfigurationProperty } from './types/configuration-property';


export class ConfigurationHandler {
	private m_configChanged: vscode.EventEmitter<string[]>;
	private m_configuration: Configuration;
	private m_changeIdents: string[];

	public constructor(private disposables: vscode.Disposable[]) {
		this.m_configChanged = new vscode.EventEmitter<string[]>();
		this.m_configuration = new Configuration();
		this.m_changeIdents = [];

		this.loadConfig()

		this.disposables.push(
			vscode.workspace.onDidChangeConfiguration(this.handleChangedConfig, this)
		);
	}

	get onDidChangeConfiguration(): vscode.Event<string[]> {
		return this.m_configChanged.event;
	}

	get configuration(): Configuration {
		return this.m_configuration;
	}

	private loadConfig(): boolean {
		let config = vscode.workspace.getConfiguration("seito-quickopen");
		if (config) {
			this.m_changeIdents = [];
			this.setChangeConfigDate<boolean>(config, "use-file-events", this.m_configuration.UseFileEvents);
			return true;
		}
		return false;
	}
	private handleChangedConfig(): void {
		if (this.loadConfig())
			this.m_configChanged.fire(this.m_changeIdents);
	}

	private setChangeConfigDate<T>(config: vscode.WorkspaceConfiguration, descriptor: string, configValue: ConfigurationProperty<T>): boolean {
		if (config.has(descriptor)) {
			configValue.Value = config.get(descriptor) as T;
			if (configValue.Changed) {
				this.m_changeIdents.push(descriptor);
				return true;
			}
		}
		return false;
	}
}