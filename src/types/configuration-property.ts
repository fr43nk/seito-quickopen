'use strict';

export class ConfigurationProperty<T>{
	private m_changed: boolean;

	public constructor(private m_prop: T) {
		this.m_changed = true;
	}

	get Value(): T {
		return this.m_prop;
	}

	get Changed(): boolean {
		let old = this.m_changed;
		this.m_changed = false;
		return old;
	}

	set Value(value:T) {
		if( this.m_prop != value )
		{
			this.m_prop = value;
			this.m_changed = true;
		}
	}
}