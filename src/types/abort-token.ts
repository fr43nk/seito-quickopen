import { Observable, Subject } from 'rxjs';

export enum AbortState_e {
	None,
	Abort,
	AbortAndRestart,
	FinishDoRestart,
	Finish
}

export class AbortToken {
	private m_state: AbortState_e;
	private m_abort$: Subject<AbortState_e>;

	constructor() {
		this.m_state = AbortState_e.None;
		this.m_abort$ = new Subject<AbortState_e>();
	}

	public get Abort(): AbortState_e {
		return this.m_state;
	}

	public doAbortAndRestart() {
		if(this.m_state === AbortState_e.None)
		{
			this.m_state = AbortState_e.AbortAndRestart;
			this.m_abort$.next(this.m_state);
		}
	}

	public doAbort() {
		if(this.m_state === AbortState_e.None)
		{
			this.m_state = AbortState_e.Abort;
			this.m_abort$.next(this.m_state);
		}
	}

	public doAbortFinish() {
		switch(this.m_state)
		{
			case AbortState_e.Abort:
			{
				this.m_abort$.next(AbortState_e.Finish);
				this.m_state = AbortState_e.None;
				break;
			}
			case AbortState_e.AbortAndRestart:
			{
				this.m_abort$.next(AbortState_e.FinishDoRestart);
				this.m_state = AbortState_e.None;
				break;
			}
			default:
			{
				this.m_state = AbortState_e.None;
			}
		}
	}

	public onAbort(): Observable<AbortState_e> {
		return this.m_abort$.asObservable();
	}
}