import * as vscode from 'vscode';
import * as path from 'path';

export class ListItem implements vscode.QuickPickItem {
  public label: string;
  public description: string;
  public fullPath: string;
  constructor(element:string) {
    this.label = '$(file) ' + path.basename(element);
    this.description = path.dirname(element);
    this.fullPath = element;
  }
}
